# -*- coding: utf-8 -*-

import os
import microblog
import unittest
import tempfile
from flask import session


class FlaskrTestCase(unittest.TestCase):

    def setUp(self):
        """
        Metoda wywoływana przed uruchomieniem każdego testu.
        Ustawia aplikację do testów i tworzy testową bazę danych.
        """
        self.app = microblog.app
        self.app.config['TESTING'] = True
        self.client = self.app.test_client()
        # stworzenie tymczasowego pliku na bazę testową
        # i pobranie uchwytu do pliku oraz jego nazwy
        self.db_fd, self.app.config['DATABASE'] = tempfile.mkstemp()
        # inicjalizacja bazy
        microblog.init_db()

    def tearDown(self):
        """
        Metoda uruchamiana po zakończeniu każdego testu.
        Czyści testową bazę danych.
        """
        # zamknięcie tymczasowego pliku z bazą testową
        os.close(self.db_fd)
        # skasowanie tymczasowego pliku z bazą testową
        os.unlink(self.app.config['DATABASE'])

    def test_database_setup(self):
        """
        Testuje czy baza została poprawnie zainicjalizowana.
        """
        con = microblog.connect_db()
        # Pobranie informacji o tebeli entries
        cur = con.execute('PRAGMA table_info(entries);')
        rows = cur.fetchall()
        # Tabela entries powinna mieć 3 kolumny
        self.assertEquals(len(rows), 3)

    def test_write_entry(self):
        """
        Testuje dodawanie nowego wpisu do bazy.
        """
        expected = (u"Tytuł", u"Zawartość")
        # Symulacja żądania z URL = /
        with self.app.test_request_context('/'):
            # Wywołanie preprocesingu dekoratorów
            self.app.preprocess_request()
            # Dodanie nowego wpisu za pomocą dedykowanej funkcji
            microblog.write_entry(*expected)
            # Odczyt z bazy przez DB API
            con = microblog .connect_db()
            cur = con.execute("select * from entries;")
            rows = cur.fetchall()
        # Powinien być tylko jeden wpis
        self.assertEquals(len(rows), 1)
        # Sprawdzenie poprawności odczytu
        for val in expected:
            self.assertTrue(val in rows[0])

    def test_get_all_entries_empty(self):
        """
        Testuje brak wpisów w nowej bazie.
        """
        # Symulacja żądania z URL = /
        with self.app.test_request_context('/'):
            # Wywołanie preprocesingu dekoratorów
            self.app.preprocess_request()
            # Odczyt wszystkich wpisów za pomocą dedykowanej funkcji
            entries = microblog.get_all_entries()
            # Nie powinno być żadnych wpisów
            self.assertEquals(len(entries), 0)

    def test_get_all_entries(self):
        """
        Testuje odczyt z bazy wszystkich wpisów.
        """
        expected = (u"Tytuł", u"Zawartość")
        # Symulacja żądania z URL = /
        with self.app.test_request_context('/'):
            # Wywołanie preprocesingu dekoratorów
            self.app.preprocess_request()
            # Dodanie nowego wpisu za pomocą dedykowanej funkcji
            microblog.write_entry(*expected)
            # Odczyt wszystkich wpisów za pomocą dedykowanej funkcji
            entries = microblog.get_all_entries()
            # Powinien być tylko jeden wpis
            self.assertEquals(len(entries), 1)
            # Sprawdzenie poprawności odczytu
            for entry in entries:
                self.assertEquals(expected[0], entry['title'])
                self.assertEquals(expected[1], entry['text'])

    def test_empty_listing(self):
        """
        Testuje odpowiedź strony głównej przy braku wpisów.
        """
        response = self.client.get('/')
        assert u'Jak dotąd brak wpisów' in unicode(response.data, 'utf-8')

    def test_listing(self):
        """
        Testuje odpowiedź strony głównej ze znanym wpisem.
        """
        expected = (u"Tytuł", u"Zawartość")
        # Symulacja żądania z URL = /
        with self.app.test_request_context('/'):
            # Wywołanie preprocesingu dekoratorów
            self.app.preprocess_request()
            # Dodanie nowego wpisu za pomocą dedykowanej funkcji
            microblog.write_entry(*expected)
        response = self.client.get('/')
        # Sprawdzenie poprawności odczytu
        for value in expected:
            assert value in unicode(response.data, 'utf-8')

    def test_login_passes(self):
        """
        Testuje poprawne logowanie.
        """
        # Symulacja żądania z URL = /
        with self.app.test_request_context('/'):
            # Wywołanie preprocesingu dekoratorów
            self.app.preprocess_request()
            # Logowanie za pomocą dedykowanej funkcji z poprawnymi danymi
            microblog.do_login(self.app.config['USERNAME'],
                               self.app.config['PASSWORD'])
            self.assertTrue(session.get('logged_in', False))

    def test_login_fails(self):
        """
        Testuje błędne logowanie.
        """
        # Symulacja żądania z URL = /
        with self.app.test_request_context('/'):
            # Wywołanie preprocesingu dekoratorów
            self.app.preprocess_request()
            # Logowanie za pomocą dedykowanej funkcji z błędnymi danymi
            self.assertRaises(ValueError, microblog.do_login,
                              self.app.config['USERNAME'],
                              'BledneHaslo')

    def login(self, username, password):
        """
        Funkcja pomocnicza do logowania.
        """
        return self.client.post('/login', data=dict(
            username=username,
            password=password
        ), follow_redirects=True)

    def logout(self):
        """
        Funkcja pomocnicza do wylogownia.
        """
        return self.client.get('/logout',
                               follow_redirects=True)

    def test_login_logout(self):
        """
        Testuje cały proces logowania z poprawnymi i błędnymi danymi.
        """
        response = self.login(self.app.config['USERNAME'],
                              self.app.config['PASSWORD'])
        assert u'Zostałeś zalogowany' in unicode(response.data, 'utf-8')
        response = self.logout()
        assert u'Zostałeś wylogowany' in unicode(response.data, 'utf-8')
        response = self.login('blad',
                              self.app.config['PASSWORD'])
        assert u'Błędne dane' in unicode(response.data, 'utf-8')
        response = self.login(self.app.config['USERNAME'],
                              'blad')
        assert u'Błędne dane' in unicode(response.data, 'utf-8')

    def test_add_entries(self):
        """
        Testuje dodawanie wpisu za pomocą widoku
        po uporzednim zalogowaniu się.
        """
        self.login(self.app.config['USERNAME'],
                   self.app.config['PASSWORD'])
        response = self.client.post('/add', data=dict(
            title=u'Cześć',
            text=u'To jest treść wpisu'
        ), follow_redirects=True)
        assert u'Jak dotąd brak wpisów' not in unicode(response.data, 'utf-8')
        assert u'Cześć' in unicode(response.data, 'utf-8')
        assert u'To jest treść wpisu' in unicode(response.data, 'utf-8')


if __name__ == '__main__':
    unittest.main()