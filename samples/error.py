# -*- coding: utf-8 -*-

from flask import Flask

app = Flask(__name__)

@app.route('/')
def hello_world():
    foo = 1 / 0
    return 'Witaj świecie!'

if __name__ == '__main__':
    app.run(debug=True)